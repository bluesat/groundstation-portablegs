#!/bin/bash

#Check if this script is running inside portablegs repo and quit
if [ ${PWD##*/} = 'groundstation-portablegs' ]
then
    echo 'Copy paste install.sh and install.list outside of groundstation-portablegs folder and run it again'
    exit 1
fi

echo 'Installing dependencies'

#Install dependencies from Ubuntu repository
echo 'Installing dependencies from Ubuntu repo'
cat install.list | xargs sudo apt -y install

#install dependencies from PIP repo
echo 'Installing dependencies from PIP repo'
sudo pip3 install aprslib

#Check if multimon-ng is installed, if its not installed then clone, compile and install
if hash multimon-ng; then
    echo 'multimon-ng is already installed'
else
    echo 'Compiling and installing multimon-ng'
    git clone https://github.com/EliasOenal/multimon-ng.git
    cd multimon-ng
    mkdir build
    cd build
    cmake ..
    make -j4
    sudo make install
fi

echo 'Dependencies install complete'