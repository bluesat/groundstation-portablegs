#Imports

import subprocess
from time import sleep
import atexit
import os
import signal

#System configuration

#Serial interface for Yaesu rigctl
SERIAL = '/dev/ttyUSB0'
SERIAL_RATE = 4800

#Audio interface for DTMF transceiver
AUDIO_OUT = ''
AUDIO_IN = ''

#Cleanup script: Kill all subprocess when this script is closed
def cleanup():
    os.killpg(0, signal.SIGKILL)

os.setpgrp()
atexit.register(cleanup)


#Generate a named pipe file
try:
    os.mkfifo('pipe')
except OSError:
    os.remove('pipe')
    os.mkfifo('pipe')

#Compile Gnuradio flowgraph into Python script
subprocess.run(["grcc", "demod.grc", "-d", "./"])

print("Launching APRS decoder script")
aprs = subprocess.Popen(['xfce4-terminal', '--title=APRS Decoder', '-x',"python3","aprs2.py"])
print(f"APRS PID is {aprs.pid}")

'''
print("Launching SDR decoder flowchart")
gr  = subprocess.Popen("./top_block.py")
print(f"Gnuradio PID is {gr.pid}")
'''

print("Launching map display")
map_v = subprocess.Popen(["python3","./pymap/layout.py"])
print(f"Map PID is {map_v.pid}")




sleep(1)
while True:
    dtmf = None
    print("")
    print("Commands")
    print("1 Send arbitrary DTMF tone")
    print("2 Terminate balloon")
    cmd = input("Enter command number [1-2]: ")
    if (cmd == '1'):
        dtmf = int(input("Enter DTMF number: "))
        print(f"Sending DTMF code {dtmf}")
    elif (cmd == '2'):
        dtmf = 7777777
        print("Sending termination code 7777777")
    else:
        print("Invalid command")
    if dtmf is not None:
        subprocess.run(["python3","dtmf.py",f"--serial={SERIAL}", f"--rate={SERIAL_RATE}",f"{dtmf}"])
        print("DTMF transmission complete")