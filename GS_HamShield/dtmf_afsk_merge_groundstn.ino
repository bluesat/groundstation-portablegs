// To do:
// It seems that although the radio can recieve afsk, when it has done some other things first it might lose this ability, although its hard to test since I only have one hamshield to work with
// Actually this might be fixed now, since I called the afsksetup with incorrect syntax that the compiler didnt care about
// Implement a way to listen for dtmf

#include <HamShield.h>
#include <DDS.h>
#include <packet.h>
#include <avr/wdt.h> 


// create object for radio
HamShield radio;
DDS dds;
AFSK afsk;
String messagebuff = "";
String origin_call = "";
String destination_call = "";
String textmessage = "";
int msgptr = 0;
int mode;

#define LED_PIN 13

#define MIC_PIN 3
#define RESET_PIN A3
#define SWITCH_PIN 2

uint32_t freq;

void setup() {
  // NOTE: if not using PWM out, it should be held low to avoid tx noise
  pinMode(MIC_PIN, OUTPUT);
  digitalWrite(MIC_PIN, LOW);
  
  // prep the switch
  pinMode(SWITCH_PIN, INPUT_PULLUP);
  
  // set up the reset control pin
  // set up the reset control pin
  pinMode(RESET_PIN, OUTPUT);
  // turn on the radio
  digitalWrite(RESET_PIN, HIGH);
  delay(5); // wait for device to come up
  
  
  // initialize serial communication
  Serial.begin(9600);
  
  Serial.println("beginning radio setup");

  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(radio.testConnection() ? "HamShield connection successful" : "HamShield connection failed");

  
}

void loop() {
  afskModeSetup();
  afskMode();
}

void afskModeSetup () {
  Serial.print("Changing to afsk mode settings. ");

    // Change radio settings to stuff suitable for afsk
  
      radio.initialize();
      radio.frequency(440125); 
      radio.setRfPower(0);
      radio.setVolume1(0xFF);
      radio.setVolume2(0xFF);
      radio.setSQHiThresh(-100);
      radio.setSQLoThresh(-100);
      //radio.setSQOn();
      radio.bypassPreDeEmph();
      dds.start();
      afsk.start(&dds);
      delay(100);
      radio.setModeReceive();
      Serial.println("Finished changing to afsk settings.");
  }

void dtmfModeSetup() {
  radio.initialize();

  Serial.print("Changing to dtmf settings. ");

  //Serial.println("setting squelch");

  radio.setSQHiThresh(-10);
  radio.setSQLoThresh(-30);
  //Serial.print("sq hi: ");
  //Serial.println(radio.getSQHiThresh());
  //Serial.print("sq lo: ");
  //Serial.println(radio.getSQLoThresh());
  radio.setSQOn();
  //radio.setSQOff();

  //Serial.println("setting frequency to: ");
  freq = 440125; // 70cm calling frequency
  radio.frequency(freq);
  //Serial.print(radio.getFrequency());
  //Serial.println("kHz");
  
  // set RX volume to minimum to reduce false positives on DTMF rx
  radio.setVolume1(6);
  radio.setVolume2(0);
  
  // set to receive
  radio.setModeReceive();
  
  radio.setRfPower(0);
    
  // configure Arduino LED for
  pinMode(LED_PIN, OUTPUT);

  // set up DTMF
  radio.enableDTMFReceive();
  
  /* DTMF timing settings are optional.
   * These times are set to default values when the device is started.
   * You may want to change them if you're DTMF receiver isn't detecting
   * codes from the HamShield (or vice versa).
   */
  radio.setDTMFDetectTime(24); // time to detect a DTMF code, units are 2.5ms
  radio.setDTMFIdleTime(50); // time between transmitted DTMF codes, units are 2.5ms
  radio.setDTMFTxTime(60);
  Serial.println("Finished changing to dtmf settings.");
  }

void afskMode () {
  
    // The "main" loop of this program is inside the afsk function because of how this program was hacked together. It could be changed to actually run in the main loop, but this works
    // fine and isn't too hard to understand since afsk is the default mode anyway

    Serial.println("To send afsk, enter something in this format:");
    Serial.println("* KG7OGM,KG7OGM,:Hi there`");
    Serial.println("To send dtmf. enter a valid dtmf code with a % at the end.");
    
    while (true) {
      
       if(Serial.available()) { 
        
        char temp = (char)Serial.read();

        if(temp == '%'){
            Serial.print("You are about to send ");
            Serial.print(messagebuff);
            Serial.println(" as a dtmf code.");
            dtmfMode(messagebuff);
            afskModeSetup();
            Serial.println("You have returned from dtmf mode.");
            messagebuff = "";
          }
        
        if(temp == '`') { 

          //temp == '`'

          delay(1000);
          
          // Serial.println(messagebuff);
          prepMessage(); 
          msgptr = 0; 
          messagebuff = "";
          Serial.print("!!");
          
        } else if (temp != '%' && temp != '\n') { 
          
          messagebuff += temp;
          msgptr++;
          
        }
      }
      
      if(msgptr > 254) {
        
        messagebuff = ""; Serial.print("X!");
        
      }
          
      if(afsk.decoder.read() || afsk.rxPacketCount()) {
        
        // A true return means something was put onto the packet FIFO
        // If we actually have data packets in the buffer, process them all now
        
        while(afsk.rxPacketCount()) {
          
          AFSK::Packet *packet = afsk.getRXPacket();
          Serial.print(F("Packet: "));
          
          if(packet) {
            
            packet->printPacket(&Serial);
            AFSK::PacketBuffer::freePacket(packet);
            
          }
        }
      }
    }
  }

void dtmfMode (String dtmfcode) {
  
      dtmfModeSetup();
      
      // If the function was called, then we must be sending the code. The if statement is legacy code but could used in the future when listening for dtmf becomes important
      
      if (true) {
        // get first code
        uint8_t code = radio.DTMFchar2code(dtmfcode[0]); // most of the codes are converted to dtmf in the loop, but the first needs to happen here for some reason
        // old debugging code
        // Serial.println("We got here");
        // Serial.print("The length of the string is: ");
        // Serial.println(dtmfcode.length());
        // Serial.print("The string is: ");
        // Serial.println(dtmfcode);
        // start transmitting
        radio.setDTMFCode(code); // set first
        radio.setTxSourceTones();
        radio.setModeTransmit();
        delay(300); // wait for TX to come to full power
        
        bool dtmf_to_tx = true;
        int counter = 1;
        while (dtmf_to_tx) {
          // wait until ready
          while (radio.getDTMFTxActive() != 1) {
            // wait until we're ready for a new code
            delay(10);
          }
          if (counter < dtmfcode.length()) {
            code = radio.DTMFchar2code(dtmfcode[counter]);
            counter++;
            if (code == 255) code = 0xE; // throw a * in there so we don't break things with an invalid code
            radio.setDTMFCode(code); // set first
          } else {
            dtmf_to_tx = false;
            break;
          }
    
          while (radio.getDTMFTxActive() != 0) {
            // wait until this code is done
            delay(10);
          }
    
        }
        // done with tone
        radio.setModeReceive();
        radio.setTxSourceMic();
      }
      
      return;
  }

void dtmfListen () {
  dtmfModeSetup();
            
  // look for tone
  char m = radio.DTMFRxLoop();
  if (m != 0) {
    Serial.print(m);
  }
}

void prepMessage() { 
   radio.setModeTransmit();
  delay(1000);
  origin_call = messagebuff.substring(0,messagebuff.indexOf(','));                                          // get originating callsign
  destination_call = messagebuff.substring(messagebuff.indexOf(',')+1,messagebuff.indexOf(',',messagebuff.indexOf(',')+1)); // get the destination call
  // textmessage = messagebuff.substring(messagebuff.indexOf(":")+1); old working code
  textmessage = "test message";
  
 // Serial.print("From: "); Serial.print(origin_call); Serial.print(" To: "); Serial.println(destination_call); Serial.println("Text: "); Serial.println(textmessage);

  AFSK::Packet *packet = AFSK::PacketBuffer::makePacket(22 + 32);

  packet->start();
  packet->appendCallsign(origin_call.c_str(),0);
  packet->appendCallsign(destination_call.c_str(),15,true);   
  packet->appendFCS(0x03);
  packet->appendFCS(0xf0);
  packet->print(textmessage);
  packet->finish();

  bool ret = afsk.putTXPacket(packet);

  if(afsk.txReady()) {
    Serial.println(F("txReady"));
    radio.setModeTransmit();
    //delay(100);
    if(afsk.txStart()) {
      Serial.println(F("txStart"));
    } else {
      radio.setModeReceive();
    }
  }
  // Wait 2 seconds before we send our beacon again.
  Serial.println("tick");
  // Wait up to 2.5 seconds to finish sending, and stop transmitter.
  // TODO: This is hackery.
  for(int i = 0; i < 500; i++) {
    if(afsk.encoder.isDone())
       break;
    delay(50);
  }
  Serial.println("Done sending");
  radio.setModeReceive();
} 
 
// I have no idea what these do

ISR(TIMER2_OVF_vect) {
  TIFR2 = _BV(TOV2);
  static uint8_t tcnt = 0;
  if(++tcnt == 8) {
    dds.clockTick();
    tcnt = 0;
  }
}

ISR(ADC_vect) {
  static uint8_t tcnt = 0;
  TIFR1 = _BV(ICF1); // Clear the timer flag
  dds.clockTick();
  if(++tcnt == 1) {
    afsk.timer();
    tcnt = 0;
  }
}
