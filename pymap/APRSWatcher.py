# For Python 3.0 and later
import requests

import json
import sys
import os
import time
import threading
from datetime import datetime
import logging

class APRSWatcher() :
    #Note that if multiple instances of this class are being used, it would be more efficient and api-friendly
    #to combine requests in a singleton class, provided the same request period is required

    def __init__(self, callsigns, period) :

        API_KEY = "126374.Kn106lRKqRDrlZg"
        self._initialPeriod = period
        self._period = period

        if isinstance(callsigns, list) :
            
            callsignStr = ",".join(callsigns)
            self._lastTx = dict.fromkeys(callsigns, 0)
        else :
            callsignStr = callsigns
            self._lastTx = {callsigns: 0}
        
        self._saveDir = os.path.dirname(os.path.realpath(__file__))+"/aprs_dump/"+callsignStr+".jsn"    #it's almost json

        self._url = f"https://api.aprs.fi/api/get?name={callsignStr}&what=loc&apikey={API_KEY}&format=json"

        logging.disable(level=50)

    def poll_database(self) :

        try:
            return requests.get(self._url).json()
        except Exception as err:
            # could not reach the API
            #sys.stdout.write(f"ERROR retrieving data: {err}")
            return None

    def _save_message(self, dic) :

        with open(self._saveDir, "a+") as f :
            f.write(json.dumps(dic,separators=(',', ':'))+",\n")

    def _convertMsg(self, dic) :
        dic["lng"] = float(dic["lng"])
        dic["lat"] = float(dic["lat"])
        dic["time"] = int(dic["lasttime"])
        if "altitude" in dic :
            dic["alt"] = float(dic.pop("altitude"))
        
        return dic

    def _update(self) :

        while True :

            result = self.poll_database()

            if result == None:
                print("Failed to reach aprs.fi at "+datetime.now().strftime('%H:%M:%S'))
                self._period *= 1.5

            elif result["result"] == "fail" :

                print("Failed: "+result["description"])
                self._period *= 2
            
            elif result["result"] == "ok" and len(result["entries"]) > 0:
                saveMessage = False
                for msg in result["entries"] :

                    lasttime = int(msg["lasttime"])
                    if lasttime > self._lastTx[msg["name"]] :
                        # if the packet is more recent
                        self._lastTx[msg["name"]] = lasttime
                        self._callback(self._convertMsg(msg))
                        saveMessage = True
                
                if saveMessage :
                    self._save_message(result)
                    self._period = self._initialPeriod
            
            time.sleep(self._period)

    def start(self, callback) :

        self._callback = callback
        threading.Thread(target=self._update, daemon=True).start()