import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQuick.Window 2.2
import QtLocation 5.6
import QtPositioning 5.6
import QtGraphicalEffects 1.12

Rectangle {
    id: panel
    height: childrenRect.height
    anchors.right: background.right

    property var callsign: ""
    property alias togSimButton: togSimButton       //used to extract current toggle state of button
    property alias togHeatButton: togHeatButton
    property alias currVel: currVel.vel             //used to set readout
    property alias currTemp: currTemp.temp

    Rectangle {
        // telemetry visualisation
        id: telemetry
        width: Screen.width/3
        height: Screen.height/5
        color: "#C5FAFA"
        radius: 6
        border.color: "black"
        border.width: 1
        Text {
            id: title
            text: "Sensor data"
            font.pixelSize: 24
            anchors.horizontalCenter: telemetry.horizontalCenter
            anchors.top: telemetry.top
        }
        Text {
            id: currVel
            property var vel: 3
            text: "Velocity\n   "+parseFloat(vel)+" ms⁻¹"
            anchors.top: title.bottom
            anchors.left: parent.left
            font.pixelSize: 18
            color: "black"
        }
        Text {
            id: currTemp
            property var temp: -10
            text: "Temp:\n   "+parseFloat(temp)+" ºC"
            font.pixelSize: 18
            anchors.top: currVel.bottom
        }

        Timer {
            interval: 1000; running: true; repeat: true
            onTriggered: lastUpdate.seconds+=1
        }
        Text {
            id: lastUpdate
            property var seconds: 0
            text: "last updated "+seconds+"s ago"
            font.pixelSize: 12
            anchors.bottom: telemetry.bottom
            anchors.right: telemetry.right
        }
    }
    Rectangle {
        // section with buttons
        id: controls
        width: Screen.width/3
        height: childrenRect.height + 2
        anchors.top: telemetry.bottom
        border.width: 1
        radius: 8
        border.color: "black"
        color: "#F2962C"

        PanelButton {
            id: runSimButton
            text: "run sim for \n" + panel.callsign + " (r)"
            anchors.left: controls.left
            onClicked: {
                eval(panel.callsign+".runSimulation()");        // yes I know, this has great abuse potential. Will work with sensible callsigns.
                map.heatImg.visible = false;
            }
        }
        Action {
            shortcut: "r"
            onTriggered : runSimButton.onClicked();
        }
        AnimatedImage {
            id : loadingImg
            anchors.left: runSimButton.right
            source: "loading.gif"
            width: 100
            height: 100
            visible: false
            speed: 2
        }
        PanelButton {
            id: togSimButton
            property var showing: true
            text: "toggle sim for \n" + panel.callsign
            anchors.horizontalCenter: controls.horizontalCenter
            onClicked: {

                showing = !showing
                map.simVisible(callsign, showing);
            }
        }

        PanelButton {
            id: simConfigButton
            text: "Simulation settings (m)"
            anchors.horizontalCenter: controls.horizontalCenter
            anchors.top: togSimButton.bottom
            onClicked: simConfigDialog.open()
            Action {
                shortcut: "m"
                onTriggered : simConfigButton.onClicked();
            }
        }

        PanelButton {
            id: togHeatButton
            text: "toggle heatmap for \n" + panel.callsign +" (h)"
            property var showing: false
            anchors.right: controls.right
            
            onClicked: {
                
                showing = !showing;
                map.heatVisible(callsign, showing);
            }
            Action {
                shortcut: "h"
                onTriggered : togHeatButton.onClicked();
            }
        }
        PanelButton {
            id: terminationSimButton

            text: "Simulate termination for \n" + panel.callsign + " (t)"
            anchors.top: togHeatButton.bottom
            Component.onCompleted: background.color = "red";
            
            onClicked: {
                eval(panel.callsign+".runTerminationSim()");
                map.heatImg.visible = false;
            }
            Action {
                shortcut: "t"
                onTriggered : terminationSimButton.onClicked();
            }
        }
        PanelButton {
            text: "TERMINATE \n" + panel.callsign
            anchors.top: terminationSimButton
            .bottom

            Component.onCompleted: {
                background.color = "red";
                radius: 10
            }
            onClicked: {
                
            }
        }
    }
    Rectangle {
        // heartbeat for data sources
        id: sources
        width: Screen.width/3
        //height: childrenRect.height + 2
        anchors.top: controls.bottom
        border.width: 1
        radius: 8
        border.color: "black"
        Column {
            spacing: 10
            Row {
                Timer {
                    id: obcTimer
                    interval: 1000; running: true; repeat: true
                    onTriggered: obcLastUpdate.seconds+=1
                }
                spacing: 10
                // Rectangle {
                //     width: 20
                //     height: 20
                //     color: "orange"
                //     border.width: 2
                //     border.color: "blue"
                //     smooth: true
                //     radius: 3
                // }
                Text {
                    text: "OBC"
                    font.pixelSize: 20
                }
                Text {
                    id: obcLastUpdate
                    font.pixelSize: 20
                    property var seconds: 0
                    text: seconds + "s ago"
                }
            }
            Row {
                Timer {
                    id: aprsTimer
                    interval: 1000; running: true; repeat: true
                    onTriggered: aprsLastUpdate.seconds+=1
                }
                spacing: 10
                Rectangle {
                    width: 20
                    height: 20
                    color: "orange"
                    border.width: 2
                    border.color: "blue"
                    smooth: true
                    radius: 3
                }
                Text {
                    text: "APRS"
                    font.pixelSize: 20
                }
                Text {
                    id: aprsLastUpdate
                    font.pixelSize: 20
                    property var seconds: 0
                    text: seconds + "s ago"
                }
            }
            Row {
                Timer {
                    id: iridiumTimer
                    interval: 1000; running: true; repeat: true
                    onTriggered: iridiumLastUpdate.seconds+=1
                }
                spacing: 10
                Rectangle {
                    width: 20
                    height: 20
                    color: "white"
                    border.width: 2
                    border.color: "blue"
                    smooth: true
                    radius: 10
                }
                Text {
                    text: "Iridium"
                    font.pixelSize: 20
                }
                Text {
                    id: iridiumLastUpdate
                    font.pixelSize: 20
                    property var seconds: 0
                    text: seconds + "s ago"
                }
            }
            Row {
                Timer {
                    id: spotTimer
                    interval: 1000; running: true; repeat: true
                    onTriggered: spotLastUpdate.seconds+=1
                }
                spacing: 10
                Rectangle {
                    width: 20
                    height: 20
                    color: "green"
                    border.width: 2
                    border.color: "blue"
                    smooth: true
                    radius: 10
                }
                Text {
                    text: "SPOT"
                    font.pixelSize: 20
                }
                Text {
                    id: spotLastUpdate
                    font.pixelSize: 20
                    property var seconds: 0
                    text: seconds + "s ago"
                }
            }
        }
    }
    
    function giveSimParameters(values) {
        simConfigDialog.setFields(values);
    }

    function giveTelemetry(values) {
        // insert values into appropriate fields
        // call reset() on the timer
    }

    function signalPing(source) {
        switch (source) {
            case "APRS":
                aprsLastUpdate.seconds = 0;
                aprsTimer.restart();
            break;
            case "Iridium" :
                iridiumLastUpdate.seconds = 0;
                iridiumTimer.restart();
            break;
            case "SPOT" :
                spotLastUpdate.seconds = 0;
                spotTimer.restart();
            break;
            case "OBC" :
                obcLastUpdate.seconds = 0;
                obcTimer.restart();
            break;
        }
    }
    ConfigDialog {
        id: simConfigDialog
        title: "Sim settings for \n"+callsign
    }

    function setLoadingStatus(val) {
        loadingImg.visible = val;
    }
}

