import QtQuick 2.0
import QtQuick.Controls 2.5

RoundButton {
    height: 100
    background: Rectangle {
        id: rect
        radius: 3
        color: "light grey"
        border.color: "black"

        SequentialAnimation {
            id: anim
            PropertyAnimation {
                id: from
                target: rect
                property: "color"
                to: "green"
                duration: 200
            }
            PropertyAnimation {
                target: rect
                property: "color"
                to: rect.color
                duration: 200
            }
        }
    }
    onClicked: {
        anim.running = true;
    }
}