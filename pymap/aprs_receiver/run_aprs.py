#!/usr/bin/env python3

# Self-contained APRS (local RF receiver) module.
# Self-manages the start-up of its dependencies.
#
# Linux dependencies: process groups, GNURadio (python2), multimon-ng
#
# If a process is lingering (which it shouldn't be), use the following:
# ps aux | grep aprs
# ps aux | grep multimon
# kill <PID>

# For UI usage, see _present_aprs() for what fields may be available
# in the dictionary sent to the callback function (1 arg).

import aprslib
import time

import subprocess as sp
import atexit
import os
import signal

import threading

# Byonics MicroTrak constants
BYONICS_VOLTAGE_B = 0.0293
BYONICS_TEMP_B = 0.489
BYONICS_TEMP_C = -273.15

# If APRS telemetry available, then Byonics MicroTrak 
# telemetry configuration is assumed.

class AprsRfReceiver():
    GR_FILE_NAME = 'aprs_demod' # filetype not included
    FIFO_PIPE_NAME = 'pipe'
    ENABLE_MULTIMON_OSCILLOSCOPE = False

    def __init__(self, aprs_file_dir):
        #super(AprsRfReceiver, self).__init__()
        self.APRS_FILE_DIR = aprs_file_dir

        #Cleanup script: Kill all subprocess when this script is closed
        # (NOTE: use __del__ class destructor instead?)
        def cleanup():
            os.killpg(0, signal.SIGKILL)
        # ** group all subprocesses under a common subprocess group
        os.setpgrp()
        atexit.register(cleanup)

        self._setup_gnuradio_script()
    
    def _setup_gnuradio_script(self):
        # generate GNURadio script using: grcc
        # * takes input .grc file 
        # * takes output file location: -d . (specifies current dir)
        gr_graph_path = os.path.join(self.APRS_FILE_DIR, self.GR_FILE_NAME + ".grc")
        sp.run(["grcc", gr_graph_path, "-d", "."])

        # generate named pipe file (to send data from GNURadio to python)
        self.pipe_path = os.path.join(self.APRS_FILE_DIR, self.FIFO_PIPE_NAME)
        try:
            os.mkfifo(self.pipe_path)
        except OSError:
            os.remove(self.pipe_path)
            os.mkfifo(self.pipe_path)


    def start(self, callback):
        self._callback = callback
        self._start_gnuradio_script()
        threading.Thread(target=self._start_audio_decoder_loop, daemon=True).start()
        # TODO: check daemon=True does not affect atexit?

    def _start_gnuradio_script(self):
        # run GNURadio script (python2); string output piped to temp .txt file
        # (TODO: update GNURadio to python3 for better code compatibility)
        gr_script_path = os.path.join(self.APRS_FILE_DIR, self.GR_FILE_NAME + ".py")
        gr_log_path = os.path.join(self.APRS_FILE_DIR, self.GR_FILE_NAME + "_temp_log.txt")
        gr_log_fh = open(gr_log_path, 'w')
        p_gr = sp.Popen(["python2", gr_script_path], 
                        stdout=gr_log_fh, stderr=gr_log_fh)
        print("GNURadio APRS script started.")

    def _start_audio_decoder_loop(self):
        # run Linux terminal program: multimon-ng
        # -A: APRS mode,  -q: quiet
        # -a SCOPE: enables new window with oscilloscope display
        # -t raw: raw audio format
        # ./pipe: last arg is the audio input file
        multimon_cmd_prefix = ["multimon-ng", "-Aq"]
        multimon_cmd_suffix = ["-t", "raw", self.pipe_path]
        if self.ENABLE_MULTIMON_OSCILLOSCOPE:
            multimon_cmd_prefix += ["-a", "SCOPE"]

        with sp.Popen(multimon_cmd_prefix + multimon_cmd_suffix,
                        stdout=sp.PIPE, bufsize=1,
                        universal_newlines=True) as p_aprs:
            print("multimon-ng && aprslib started.")
            #for line in p_aprs.stdout: # susceptible to UnicodeDecodeError?
            while True:
                try:
                    line = p_aprs.stdout.readline()
                except Exception as e:
                    print("APRS multimon-ng error:", e)
                    continue

                try:
                    # TODO: msg logging
                    rcv_time = time.time()
                    print("Received:", line.rstrip())

                    aprsmsg = aprslib.parse(line[6:]) # removes multimon-ng: "APRS: " prefix
                    print(aprsmsg)
                    
                    # update UI
                    aprsmsg_filtered = self._present_aprs(aprsmsg, rcv_time)
                    self._callback(aprsmsg_filtered)
                    
                except Exception as e:
                    print("APRS parsing error:", e)  # TODO: use logging

    def _present_aprs(self, aprsmsg, rcv_time):
        aprsmsg_filtered = {}
        # note: keys dependent on aprslib
        copy_keys = ['from', 'latitude', 'longitude', 'altitude', 'course', 'speed']
        for k in copy_keys:
            if k in aprsmsg:
                aprsmsg_filtered[k] = aprsmsg[k]
        # add timestamps
        if 'timestamp' in aprsmsg:
            aprsmsg_filtered['tx_localtime'] = aprsmsg['timestamp']
        aprsmsg_filtered['rx_localtime'] = rcv_time
        
        if 'telemetry' in aprsmsg:
            telemetry = aprsmsg['telemetry']
            # add sequence number
            if 'seq' in telemetry:
                aprsmsg_filtered['seq'] = telemetry['seq']
            
            # convert telemetry (assumes Byonics MicroTrak configuration)
            if 'vals' in telemetry and type(telemetry['vals']) == list and len(telemetry['vals']) >= 2:
                aprsmsg_filtered['battery_voltage'] = BYONICS_VOLTAGE_B * telemetry['vals'][0]
                aprsmsg_filtered['tempC'] = BYONICS_TEMP_B * telemetry['vals'][1] + BYONICS_TEMP_C

        return aprsmsg_filtered


        # (OLD CODE)
        # time = strftime("%Y-%m-%d %H:%M:%S", localtime())
        # try:
        #     if 'altitude' in aprsmsg:
        #         alt = aprsmsg['altitude'] * 0.3 #convert feet into meter
        #         print(f"time: {time} callsign: {aprsmsg['from']} latitude: {aprsmsg['latitude']} longitude: {aprsmsg['longitude']} altitude: {alt}m")
        #         c.execute("insert into Telemetry (RxTime,Callsign,lat,lon,alt) values (?,?,?,?,?)",(time,aprsmsg['from'], aprsmsg['latitude'],aprsmsg['longitude'],alt))
        #     else:
        #         print(f"time: {time} callsign: {aprsmsg['from']} latitude: {aprsmsg['latitude']} longitude: {aprsmsg['longitude']} altitude: NA")
        #         c.execute("insert into Telemetry (RxTime,Callsign,lat,lon) values (?,?,?,?)",(time,aprsmsg['from'], aprsmsg['latitude'],aprsmsg['longitude']))
        #     socket.send_pyobj(aprsmsg)
        #     #conn.commit()
        # except KeyError:
        #     print(aprsmsg)
        #print(time.time() - start)


def main():
    aprs_file_dir = '.'
    aprs_rcvr = AprsRfReceiver(aprs_file_dir)

    def stub_callback(dic):
        print(dic)
        print("")

    aprs_rcvr.start(stub_callback)

    try:
        while True: # allow thread to run
            continue
    except KeyboardInterrupt:
        print("Ending program via KeyboardInterrupt")



if __name__ == '__main__':
    main()