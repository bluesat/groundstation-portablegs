#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Aprs Demod
# Generated: Fri Jan 17 23:55:42 2020
##################################################

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt5 import Qt
from PyQt5 import Qt, QtCore
from gnuradio import analog
from gnuradio import audio
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import osmosdr
import sip
import sys
import time
from gnuradio import qtgui


class aprs_demod(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Aprs Demod")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Aprs Demod")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "aprs_demod")

        if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
            self.restoreGeometry(self.settings.value("geometry").toByteArray())
        else:
            self.restoreGeometry(self.settings.value("geometry", type=QtCore.QByteArray))

        ##################################################
        # Variables
        ##################################################
        self.aprs_freq = aprs_freq = 145.175e6
        self.doppler_interval = doppler_interval = (350e3 / 3.6) /3e8 * aprs_freq
        self.aprs_bw = aprs_bw = 5e3
        self.filter_cutoff = filter_cutoff = aprs_bw + doppler_interval
        self.waterfall_xaxis_resize = waterfall_xaxis_resize = 2*filter_cutoff
        self.samp_rate = samp_rate = 2400000
        self.samp_decimate = samp_decimate = 10
        self.hw_rf_gain = hw_rf_gain = 50
        self.filter_trans_width = filter_trans_width = 10000
        self.audio_mute_checkbox = audio_mute_checkbox = False

        ##################################################
        # Blocks
        ##################################################
        self._waterfall_xaxis_resize_range = Range(2*filter_cutoff, 2000000, 100000, 2*filter_cutoff, 200)
        self._waterfall_xaxis_resize_win = RangeWidget(self._waterfall_xaxis_resize_range, self.set_waterfall_xaxis_resize, 'Frequency Rescale', "slider", float)
        self.top_grid_layout.addWidget(self._waterfall_xaxis_resize_win, 10, 0, 1, 9)
        [self.top_grid_layout.setRowStretch(r,1) for r in range(10,11)]
        [self.top_grid_layout.setColumnStretch(c,1) for c in range(0,9)]
        self._hw_rf_gain_range = Range(0, 50, 1, 50, 200)
        self._hw_rf_gain_win = RangeWidget(self._hw_rf_gain_range, self.set_hw_rf_gain, 'RF Gain (dB)', "counter_slider", int)
        self.top_layout.addWidget(self._hw_rf_gain_win)
        _audio_mute_checkbox_check_box = Qt.QCheckBox('Mute Audio')
        self._audio_mute_checkbox_choices = {True: True, False: False}
        self._audio_mute_checkbox_choices_inv = dict((v,k) for k,v in self._audio_mute_checkbox_choices.iteritems())
        self._audio_mute_checkbox_callback = lambda i: Qt.QMetaObject.invokeMethod(_audio_mute_checkbox_check_box, "setChecked", Qt.Q_ARG("bool", self._audio_mute_checkbox_choices_inv[i]))
        self._audio_mute_checkbox_callback(self.audio_mute_checkbox)
        _audio_mute_checkbox_check_box.stateChanged.connect(lambda i: self.set_audio_mute_checkbox(self._audio_mute_checkbox_choices[bool(i)]))
        self.top_grid_layout.addWidget(_audio_mute_checkbox_check_box, 10, 9, 1, 1)
        [self.top_grid_layout.setRowStretch(r,1) for r in range(10,11)]
        [self.top_grid_layout.setColumnStretch(c,1) for c in range(9,10)]
        self.rtlsdr_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + 'rtl=0' )
        self.rtlsdr_source_0.set_sample_rate(samp_rate)
        self.rtlsdr_source_0.set_center_freq(aprs_freq, 0)
        self.rtlsdr_source_0.set_freq_corr(0, 0)
        self.rtlsdr_source_0.set_dc_offset_mode(2, 0)
        self.rtlsdr_source_0.set_iq_balance_mode(2, 0)
        self.rtlsdr_source_0.set_gain_mode(False, 0)
        self.rtlsdr_source_0.set_gain(hw_rf_gain, 0)
        self.rtlsdr_source_0.set_if_gain(0, 0)
        self.rtlsdr_source_0.set_bb_gain(0, 0)
        self.rtlsdr_source_0.set_antenna('', 0)
        self.rtlsdr_source_0.set_bandwidth(0, 0)

        self.rational_resampler_xxx_1 = filter.rational_resampler_fff(
                interpolation=22050,
                decimation=48000,
                taps=None,
                fractional_bw=None,
        )
        self.qtgui_waterfall_sink_x_0 = qtgui.waterfall_sink_c(
        	1024, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	aprs_freq, #fc
        	waterfall_xaxis_resize, #bw
        	"Raw SDR signal", #name
                1 #number of inputs
        )
        self.qtgui_waterfall_sink_x_0.set_update_time(0.10)
        self.qtgui_waterfall_sink_x_0.enable_grid(True)
        self.qtgui_waterfall_sink_x_0.enable_axis_labels(True)

        if not True:
          self.qtgui_waterfall_sink_x_0.disable_legend()

        if "complex" == "float" or "complex" == "msg_float":
          self.qtgui_waterfall_sink_x_0.set_plot_pos_half(not True)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0.set_line_alpha(i, alphas[i])

        self.qtgui_waterfall_sink_x_0.set_intensity_range(-140, 10)

        self._qtgui_waterfall_sink_x_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_win, 0, 0, 10, 10)
        [self.top_grid_layout.setRowStretch(r,1) for r in range(0,10)]
        [self.top_grid_layout.setColumnStretch(c,1) for c in range(0,10)]
        self.qtgui_number_sink_0 = qtgui.number_sink(
            gr.sizeof_float,
            0.8,
            qtgui.NUM_GRAPH_HORIZ,
            1
        )
        self.qtgui_number_sink_0.set_update_time(0.10)
        self.qtgui_number_sink_0.set_title('APRS')

        labels = ['RSSI', '', '', '', '',
                  '', '', '', '', '']
        units = ['dB', '', '', '', '',
                 '', '', '', '', '']
        colors = [("blue", "red"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(1):
            self.qtgui_number_sink_0.set_min(i, -100)
            self.qtgui_number_sink_0.set_max(i, 0)
            self.qtgui_number_sink_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0.set_label(i, labels[i])
            self.qtgui_number_sink_0.set_unit(i, units[i])
            self.qtgui_number_sink_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0.enable_autoscale(False)
        self._qtgui_number_sink_0_win = sip.wrapinstance(self.qtgui_number_sink_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_number_sink_0_win)
        self.mute_audio = blocks.mute_ff(bool(audio_mute_checkbox))
        self.low_pass_filter_0 = filter.fir_filter_ccf(samp_decimate, firdes.low_pass(
        	1, samp_rate, filter_cutoff, filter_trans_width, firdes.WIN_HAMMING, 6.76))
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_float_to_short_0 = blocks.float_to_short(1, 32768)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_short*1, 'pipe', False)
        self.blocks_file_sink_0.set_unbuffered(False)
        self.blocks_complex_to_mag_squared_0 = blocks.complex_to_mag_squared(1)
        self.audio_sink_0 = audio.sink(48000, '', True)
        self.analog_nbfm_rx_0 = analog.nbfm_rx(
        	audio_rate=48000,
        	quad_rate=samp_rate/samp_decimate,
        	tau=75e-6,
        	max_dev=5e3,
          )

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_nbfm_rx_0, 0), (self.mute_audio, 0))
        self.connect((self.analog_nbfm_rx_0, 0), (self.rational_resampler_xxx_1, 0))
        self.connect((self.blocks_complex_to_mag_squared_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_float_to_short_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.qtgui_number_sink_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.analog_nbfm_rx_0, 0))
        self.connect((self.mute_audio, 0), (self.audio_sink_0, 0))
        self.connect((self.rational_resampler_xxx_1, 0), (self.blocks_float_to_short_0, 0))
        self.connect((self.rtlsdr_source_0, 0), (self.blocks_complex_to_mag_squared_0, 0))
        self.connect((self.rtlsdr_source_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.rtlsdr_source_0, 0), (self.qtgui_waterfall_sink_x_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "aprs_demod")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_aprs_freq(self):
        return self.aprs_freq

    def set_aprs_freq(self, aprs_freq):
        self.aprs_freq = aprs_freq
        self.rtlsdr_source_0.set_center_freq(self.aprs_freq, 0)
        self.qtgui_waterfall_sink_x_0.set_frequency_range(self.aprs_freq, self.waterfall_xaxis_resize)
        self.set_doppler_interval((350e3 / 3.6) /3e8 * self.aprs_freq)

    def get_doppler_interval(self):
        return self.doppler_interval

    def set_doppler_interval(self, doppler_interval):
        self.doppler_interval = doppler_interval
        self.set_filter_cutoff(self.aprs_bw + self.doppler_interval)

    def get_aprs_bw(self):
        return self.aprs_bw

    def set_aprs_bw(self, aprs_bw):
        self.aprs_bw = aprs_bw
        self.set_filter_cutoff(self.aprs_bw + self.doppler_interval)

    def get_filter_cutoff(self):
        return self.filter_cutoff

    def set_filter_cutoff(self, filter_cutoff):
        self.filter_cutoff = filter_cutoff
        self.set_waterfall_xaxis_resize(2*self.filter_cutoff)
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_trans_width, firdes.WIN_HAMMING, 6.76))

    def get_waterfall_xaxis_resize(self):
        return self.waterfall_xaxis_resize

    def set_waterfall_xaxis_resize(self, waterfall_xaxis_resize):
        self.waterfall_xaxis_resize = waterfall_xaxis_resize
        self.qtgui_waterfall_sink_x_0.set_frequency_range(self.aprs_freq, self.waterfall_xaxis_resize)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.rtlsdr_source_0.set_sample_rate(self.samp_rate)
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_trans_width, firdes.WIN_HAMMING, 6.76))

    def get_samp_decimate(self):
        return self.samp_decimate

    def set_samp_decimate(self, samp_decimate):
        self.samp_decimate = samp_decimate

    def get_hw_rf_gain(self):
        return self.hw_rf_gain

    def set_hw_rf_gain(self, hw_rf_gain):
        self.hw_rf_gain = hw_rf_gain
        self.rtlsdr_source_0.set_gain(self.hw_rf_gain, 0)

    def get_filter_trans_width(self):
        return self.filter_trans_width

    def set_filter_trans_width(self, filter_trans_width):
        self.filter_trans_width = filter_trans_width
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_trans_width, firdes.WIN_HAMMING, 6.76))

    def get_audio_mute_checkbox(self):
        return self.audio_mute_checkbox

    def set_audio_mute_checkbox(self, audio_mute_checkbox):
        self.audio_mute_checkbox = audio_mute_checkbox
        self._audio_mute_checkbox_callback(self.audio_mute_checkbox)
        self.mute_audio.set_mute(bool(self.audio_mute_checkbox))


def main(top_block_cls=aprs_demod, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
