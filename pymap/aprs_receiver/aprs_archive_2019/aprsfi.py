#aprs.fi API interface

import requests
import zmq
import sqlite3
import sys
from time import sleep

APIKEY = "130854.Hd0BywyIDPktO"
callsigns = ["VK2KWT-7", "OH7RDA"] #Use this for testing
#callsigns = ["VK2UNS", "VK2UNS-1"]
callsign = ",".join(callsigns)
delay = 1

while True:
    if (delay > 120):
        print("Too many errors")
        sys.exit(1)
    try:
        rawaprs = requests.get(f"http://api.aprs.fi/api/get?name={callsign}&what=loc&apikey={APIKEY}&format=json").json()
        if rawaprs['result'] == 'fail':
            print("Error: " + rawaprs['description'])
            print("Increasing delay")
            delay = delay * 2
        else:    
            for station in rawaprs['entries']:
                if(station["time"] != station["lasttime"]):
                    print(f"callsign: {station['name']} latitude: {station['lat']} longitude: {station['lng']}")
    except Exception as e:
        print("Error: " + str(e))
        print("Increasing delay")
        delay = delay * 2 #Exponential backoff 
    sleep(delay)

