#!/usr/bin/env python3

import aprslib
import sys
import subprocess
import sqlite3
from time import localtime, strftime
import zmq
import time

# this dumps received packets into the sql database

from subprocess import Popen, PIPE
# conn = sqlite3.connect('balloon')
# c = conn.cursor()
# port = 5556
# context = zmq.Context()
# socket = context.socket(zmq.PUB) 
# socket.bind(f"tcp://*:{port}")


#with open("pipe",'r') as fifo:
#with Popen(["multimon-ng", "-Aq", "-a", "SCOPE"], stdout=PIPE, bufsize=1, universal_newlines=True) as p:
with Popen(["multimon-ng", "-Aq", "-a", "SCOPE", "-t", "raw", "./pipe"], stdout=PIPE, bufsize=1, universal_newlines=True) as p:
    print("multimon-ng && aprslib started.")
    for line in p.stdout:
        print("")
        aprsmsg = aprslib.parse(line[6:])
        print("Received:", aprsmsg['raw'])
        print(aprsmsg)
        time = strftime("%Y-%m-%d %H:%M:%S", localtime())
        # try:
        #     if 'altitude' in aprsmsg:
        #         alt = aprsmsg['altitude'] * 0.3 #convert feet into meter
        #         print(f"time: {time} callsign: {aprsmsg['from']} latitude: {aprsmsg['latitude']} longitude: {aprsmsg['longitude']} altitude: {alt}m")
        #         c.execute("insert into Telemetry (RxTime,Callsign,lat,lon,alt) values (?,?,?,?,?)",(time,aprsmsg['from'], aprsmsg['latitude'],aprsmsg['longitude'],alt))
        #     else:
        #         print(f"time: {time} callsign: {aprsmsg['from']} latitude: {aprsmsg['latitude']} longitude: {aprsmsg['longitude']} altitude: NA")
        #         c.execute("insert into Telemetry (RxTime,Callsign,lat,lon) values (?,?,?,?)",(time,aprsmsg['from'], aprsmsg['latitude'],aprsmsg['longitude']))
        #     socket.send_pyobj(aprsmsg)
        #     #conn.commit()
        # except KeyError:
        #     print(aprsmsg)
        #print(time.time() - start)
print("It shouldnt come here")

