import aprslib
import sys
import subprocess

from subprocess import Popen, PIPE

with Popen(["./input.sh"], stdout=PIPE, bufsize=1, universal_newlines=True) as p:
    for line in p.stdout:
        print("New contents: {}".format(line))
        aprsmsg = aprslib.parse(line[6:])
        print(aprsmsg['raw'])
        print(aprsmsg)
        print(f"callsign: {aprsmsg['from']} latitude: {aprsmsg['latitude']} longitude: {aprsmsg['longitude']}")

