import json
from PIL import Image, ImageDraw
from scipy.stats import norm
from math import sqrt, cos, sin, pi, log, tan
import sys
import numpy as np

# ONLY WORKS FOR THE SOUTHERN HEMISPHERE
class Heatmap() :

    # resolution ~= pixels/degree
    # assumes the image is small enough to ignore non-parallel lines of longitude

    def __init__(self, resolution) :

        self._resolution = resolution
        self._latComp = 1

    def sphereToCart(self,lng,lat) :
        x = round(abs( ((lng-self._lngMin)/self._dLng) * (self._width-1) ))
        y = round(abs( (1-(lat-self._latMin)/self._dLat) * (self._height-1)))
        return (x,y)
    
    @property
    def pixWidth(self) :
        return self._width
    @property
    def pixHeight(self) :
        return self._height

    @property
    def topLeftAnchor(self) :
        return (self._latMax, self._lngMin)
    
    @property
    def centreAnchor(self) :
        return ((self._latMin+self._latMax)/2, (self._lngMin+self._lngMax)/2)

    @property
    def resolution(self) :
        return self._resolution

    # Expects the inputJson to be of the form:
    # { "points" : [
    # { "lat" : -32.26667, "lng" : 150.90000},
    # ...
    # ]}
    def generate(self, outputName, inputJson) :

        with open(inputJson, 'r') as f :
            sims = json.load(f)
        
        xData = np.zeros(len(sims["points"]))
        yData = np.zeros(len(sims["points"]))
        index = 0

        for entry in sims["points"] :
            xData[index] = entry["lng"]
            yData[index] = entry["lat"]
            index+=1
        margin = .1
        # the margin is so the colour is not cut off at the border
        self._latMin = min(yData)-margin
        self._latMax = max(yData)+margin
        self._lngMin = min(xData)-margin
        self._lngMax = max(xData)+margin

        self._dLng = abs(self._lngMax - self._lngMin)
        self._dLat = abs(self._latMax - self._latMin)

        self._latComp = cos(pi/180*(self._latMin+self._latMax)/2)

        self._width = int(round(self._dLng*self._resolution))
        self._height = int(round(self._dLat*self._resolution /self._latComp))
        
        img = Image.new("RGBA", (int(self.pixWidth),int(self.pixHeight)), color=(0,0,0,0))
        pixels = img.load()
        
        # convert lats and longs to x and y
        for t in range(0, len(xData)) :
            (xData[t], yData[t]) = self.sphereToCart(xData[t],yData[t])
        
        weights = np.zeros([self.pixWidth, self.pixHeight])
        
        # get pixel weights
        calcRadius = min([self.pixHeight, self.pixWidth])/4
        for p in range(0, len(xData)) :
            for x in range(int(xData[p]-calcRadius), int(xData[p]+calcRadius) ):
                for y in range(int(yData[p]-calcRadius), int(yData[p]+calcRadius)) :
                    
                    if (x > 0 and x < self.pixWidth and y > 0 and y < self.pixHeight) :
                        
                        dWeight = -((xData[p]-x)**2+(yData[p]-y)**2)/calcRadius**2+1    # quadratic in distance from pixel to point
                        if dWeight > 0 :
                            weights[x,y] += dWeight
        
        # get max and min weights (for scaling purposes)
        weightMax = 0
        weightMin = sys.float_info.max

        for x in range(0, self.pixWidth) :
            for y in range(0, self.pixHeight) :
                    if weights[x,y] > weightMax :
                        weightMax = weights[x,y]
                    elif weights[x,y] < weightMin :
                        weightMin = weights[x,y]
        
        # scale weights and paint image
        alphaMax = 128
        alphaCutoff = 0.4
        for x in range(0, self.pixWidth) :
            for y in range(0, self.pixHeight) :
        
                pixWeight = ((weights[x,y]-weightMin)/(weightMax-weightMin))    # between 0 and 1
                
                alpha = 128
        
                if pixWeight < alphaCutoff :
                    alpha = pixWeight*alphaMax/alphaCutoff
                
                # linear colour map
                green = [0, 255, 0] # green
                yellow = [255, 255, 0] # yellow
                red = [255, 0, 0] # red
                color = [0, 0, 0]
        
                for i in [0,1,2]:
                    if pixWeight < 0.5 :
                        color[i] = green[i] + 2*pixWeight * (yellow[i] - green[i])
                    else :
                        color[i] = yellow[i] + 2*(pixWeight-.5) * (red[i] - yellow[i])
        
                pixels[x,y] = (int(round(color[0])), int(round(color[1])), int(round(color[2])), int(round(alpha)))

        img.save(outputName)