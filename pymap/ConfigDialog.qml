import QtQuick 2.0
import QtQuick.Controls 2.5

Dialog {
    x: 10
    y: 10
    width: parent.width
    height: 700
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

    standardButtons: Dialog.Ok | Dialog.Cancel
    Column {
        anchors.fill: parent

        CheckBox {
            id: useLive
            text: "Use current location\n as starting point?"
            checked: true
            onClicked: {
                textLat.readOnly = checked;
                textLng.readOnly = checked;
                textElev.readOnly = checked;
            }
        }
        Text {
            text: "Launch site latitude (º)"
            color: "black"
        }
        TextField {
            id: textLat
            width: parent.width * 0.75
            selectByMouse: true
        }

        Text {
            text: "Launch site longitude (º)"
            color: "black"
        }
        TextField {
            id: textLng
            width: parent.width * 0.75
            selectByMouse: true
        }

        Text {
            text: "Launch site elevation (m)"
            color: "black"
        }
        TextField {
            id: textElev
            width: parent.width * 0.75
            selectByMouse: true
        }

        Text {
            text: "Inflation temperature (ºC)"
            color: "black"
        }

        TextField {
            id: textInfl
            width: parent.width * 0.75
            selectByMouse: true
        }

        Text {
            text: "Nozzle lift (kg)"
            color: "black"
        }
        TextField {
            id: textNzlLft
            width: parent.width * 0.75
            selectByMouse: true
        }

        Text {
            text: "Payload mass (kg)"
            color: "black"
        }
        TextField {
            id: textMass
            width: parent.width * 0.75
            selectByMouse: true
        }

        Text {
            text: "Number of runs per sim"
            color: "black"
        }
        TextField {
            id: textNSimRuns
            width: parent.width * 0.75
            selectByMouse: true
        }

        Text {
            text: "Equivalent payload sphere diameter (m)"
            color: "black"
        }
        TextField {
            id: textTrainSphere
            width: parent.width * 0.75
            selectByMouse: true
        }
    }

    onOpened: {
        
        eval(panel.callsign+".requestSimParameters()");    // this should eventually call setFields
    }

    onAccepted: {
        var params = {"launchLat": parseFloat(textLat.text),
                      "launchLng": parseFloat(textLng.text),
                      "launchElev": parseFloat(textElev.text),
                      "inflateTemp": parseFloat(textInfl.text),
                      "nozzleLift": parseFloat(textNzlLft.text),
                      "payloadMass": parseFloat(textMass.text),
                      "numSims": parseInt(textNSimRuns.text),
                      "trainSphereDia": parseFloat(textTrainSphere.text),
                      "useLive":        useLive.checked};
        
        for (var key in params) {
            if (isNaN(params[key])) {
                console.log("Invalid input for "+key+": "+ params[key]);
                return;
            }
        }
        eval(panel.callsign+".setSimParameters(params)");
    }
    
    function setFields(values) {
        textLat.text         = values["launchLat"];
        textLng.text         = values["launchLng"];
        textElev.text        = values["launchElev"];
        textInfl.text        = values["inflateTemp"];
        textNzlLft.text      = values["nozzleLift"];
        textMass.text        = values["payloadMass"];
        textNSimRuns.text    = values["numSims"];
        textTrainSphere.text = values["trainSphereDia"];
        useLive.checked      = values["useLive"];

    }
}