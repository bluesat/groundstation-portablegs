// APRS path line
import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Window 2.2
import QtLocation 5.6
import QtPositioning 5.6

MapPolyline{
    id: aprsPath
    line.width: 2
    line.color: 'green'
    path: []
}