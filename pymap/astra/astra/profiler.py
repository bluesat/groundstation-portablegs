"""
Example Sounding based Simulation

ASTRA High Altitude Balloon Flight Planner

University of Southampton
"""
from datetime import datetime
from astra.simulator import *


if __name__ == "__main__":
    simEnvironment = forecastEnvironment(launchSiteLat=-32.266667,         # deg
                                            launchSiteLon=150.9,         # deg
                                            launchSiteElev=157  ,             # m
                                            inflationTemperature=20,     # degC
                                            dateAndTime=datetime.strptime('Aug 12 2019  1:00PM', '%b %d %Y %I:%M%p'),# + timedelta(days=1),
                                            UTC_offset=0,
                                            forceNonHD=False,
                                            debugging=False)

    # load locally stored weather predictions
    # assumes is being run from the root project folder
    files = {"tmpprs":os.getcwd()+"/pymap/weather_data/tmpprs12-8-19.txt",
                "hgtprs":os.getcwd()+"/pymap/weather_data/hgtprs12-8-19.txt",
                "ugrdprs":os.getcwd()+"/pymap/weather_data/ugrdprs12-8-19.txt",
                "vgrdprs":os.getcwd()+"/pymap/weather_data/vgrdprs12-8-19.txt"}
    
    simEnvironment.loadFromNOAAFiles(files)
    # simEnvironment.load()

    # Launch setup
    simFlight = flight(environment=simEnvironment,
                    balloonGasType='Helium',
                    balloonModel='HW200',
                    nozzleLift=3.445,                                # kg
                    payloadTrainWeight=1.5,                    # kg
                    parachuteModel='SPH36',
                    numberOfSimRuns=10,
                    trainEquivSphereDiam=0.1,                    # m
                    floatingFlight=False,
                    outputFile=os.path.join('.', f"pymap/astra/astra/test_output"),
                    debugging=False,
                    log_to_file=False)

    # Run the simulation
    simFlight.run()