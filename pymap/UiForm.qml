import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Window 2.2
import QtLocation 5.6
import QtPositioning 5.6
import QtGraphicalEffects 1.12

Item {
    width: Screen.width
    height: Screen.height

    Rectangle {
        id: background

        objectName: "background"
        width: Screen.width
        height: Screen.height
        color: "#ffffff"

        property var panelComp: Qt.createComponent("balloonPanel.qml");
        property var panels: ({});
        property var currBalloon: "";

        Item {
            // the map element
            width: Screen.width*2/3
            height: Screen.height
    
            id: imap

            property alias map: map
            map.center: QtPositioning.coordinate(-32.266667,150.9)

            Plugin {
                id: mapPlugin
                name: "osm"

                PluginParameter {
                    name: "osm.mapping.custom.host"
                    value: "http://localhost:80/tile/" //"http://10.42.0.1/osm/"
                }

                PluginParameter {
                    name: "osm.mapping.providersrepository.address"
                    value: "http://localhost" //"http://10.42.0.1"
                }
            }
            BalloonMap{
                id: map
                objectName: "map"
            }
        }

        ComboBox {
            id: balloonSwitcher
            anchors.left: imap.right
            anchors.right: background.right
            model: ListModel { id: model }

            onCurrentIndexChanged: {
                
                var oldCallsign = background.currBalloon;
                var newCallsign = model.get(balloonSwitcher.currentIndex).text;
                
                background.panels[oldCallsign].visible = false;
                background.panels[newCallsign].visible = true;
                background.currBalloon = newCallsign;
                map.setCurrBalloon(newCallsign);
                map.simVisible(oldCallsign, false);
                map.simVisible(newCallsign, background.panels[newCallsign].togSimButton.showing)
                map.locVisible(oldCallsign, false);
                map.locVisible(newCallsign, true);
                map.heatVisible(oldCallsign, false);
                map.heatVisible(newCallsign, background.panels[newCallsign].togHeatButton.showing)
            }
        }

        function addBalloon(callsign) {
            
            // note: assumes the balloon has not already been added.
            // guarding against duplicate balloons would require searching the ListModel of balloonSwitcher
            panels[callsign] = panelComp.createObject(background, {callsign: callsign});
            panels[callsign].anchors.left = imap.right;
            
            panels[callsign].anchors.top = balloonSwitcher.bottom;
            panels[callsign].visible = false;
            balloonSwitcher.model.append({text:callsign});

            if (currBalloon === "") { 
                currBalloon = callsign;
                map.setCurrBalloon(callsign);
                balloonSwitcher.currentIndex = 0;
            }
        }

        function giveSimParameters(callsign, values) {
            panels[callsign].giveSimParameters(values);
        }

        function setSimulatingStatus(val) {
            panels[currBalloon].setLoadingStatus(val);
        }

    }

}