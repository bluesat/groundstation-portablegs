import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2
import QtLocation 5.6
import QtPositioning 5.6
import QtGraphicalEffects 1.12

Map {
    id: map
    objectName: "map"
    width: 640
    height: 480
    anchors.fill: parent
    plugin: mapPlugin
    zoomLevel: 14
    activeMapType: supportedMapTypes[supportedMapTypes.length-1]

    property alias heatImg: heatImg

    property var currBalloon: ""

    property var locDotComp: Qt.createComponent("locDot.qml");
    property var locLineComp: Qt.createComponent("locLine.qml");
    property var predLineComp: Qt.createComponent("predLine.qml");
    property var predDotComp: Qt.createComponent("predDot.qml");

    // a dictionary with key=callsign, value=list of MapPolyLine objects, indexed by sim run num
    property var predLines: ({})

    // a dictionary with key=callsign, value=list of dictionaries. List indexed by sim run, and each dict has dot and dot type (burst or landing)
    property var predDots: ({})

    // a dictionary with callsign and locDot object list pairs
    property var locDots: ({})

    // a dictionary with callsign and MapPolyLine object pairs
    property var locLines: ({})
    
    //takes a dictionary cotaining data from one location message
    function addDot(locData) {
        var callsign = locData["callsign"];
        if (!(locLines.hasOwnProperty(callsign))) {
            // first dot for this callsign
            var newLine = locLineComp.createObject(map);       // create the MapPolyLine for the callsign
            locLines[callsign] = newLine;                   // add the new line to the dictionary

            if (background.currBalloon == callsign)
                addMapItem(newLine);                            // add the new line to the map
            
            locDots[callsign] = [];            // initialise the list of dots
        }
        var srcString = locData["source"]

        if (locData.hasOwnProperty("id") && locData["id"] != "") {
            srcString += " ("+locData["id"]+")"
        }
        var newDot = locDotComp.createObject(map, { "coordinate.latitude": locData["lat"], 
                                                    "coordinate.longitude": locData["lng"],
                                                    "timestamp": locData["timestamp"],
                                                    "alt": locData["alt"],
                                                    "text": callsign+
                                                            "\nlat: "+locData["lat"].toFixed(4)+
                                                            "\nlng: "+locData["lng"].toFixed(4)+
                                                            "\nalt: "+locData["alt"].toFixed(4)+"m\nTime: "+locData["time"]+
                                                            "\nsrc: "+srcString});

        
        newDot.setAppearance(locData["source"]);
        locDots[callsign].push(newDot);
        locLines[callsign].addCoordinate(QtPositioning.coordinate(locData["lat"], locData["lng"]));
        var index = checkOrdering(callsign);
        background.panels[ callsign ].signalPing(locData["source"]);

        if (index < locDots[callsign].length-1) {
            // need to update dot next in list, as new dot could have been inserted behind
            var nextDot = locDots[callsign][index+1];
            if (nextDot.alt > newDot.alt)
                nextDot.setBorderColor("blue");
            else
                nextDot.setBorderColor("red");
        }
        if (locData["alt"] > getPriorAltitude(callsign, locData["timestamp"]))
            newDot.setBorderColor("blue");
        else
            newDot.setBorderColor("red");
        

        if (background.currBalloon == callsign) {
            addMapItem(newDot);
        }
    }

    function getPriorAltitude(callsign, time) {
        // helper function
        // a dot with the given time MUST exist for this to work properly
        // use binary search to track down the entry with the given time, then return the altitude of the one before
        // if the found dot is the first in the list, 0 is returned
        var list = locDots[callsign];
        var i = Math.floor(list.length/2);
        var upper = list.length;
        var lower = 0;
        while (list[i].timestamp != time) {
            i = lower + Math.floor((upper-lower)/2);
            if (list[i].timestamp < time) {
                lower = i;
            }else{
                upper = i;
            }
        }
        if (i > 0)
            return list[i-1].alt;
        else
            return 0;
    }

    function checkOrdering(callsign) {
        // ensure the mapPolyLine is joining the dots chronologically
        var list = locDots[callsign];
        var len = list.length;
        if (len > 1) {
            var index = len - 1;
            while (index > 0 && list[index].timestamp < list[index-1].timestamp) {
                // insertion sort for the last element
                var temp = list[index-1];
                list[index-1] = list[index];
                list[index] = temp;
                index--;
            }
            if (index < len-1) {
                // if sorting the mapLine is required
                var line = locLines[callsign].path;
                for (var c = line.length-1; c > index; c--) {
                    var temp = line[c-1];
                    line[c-1] = line[c];
                    line[c] = temp;
                }
                locLines[callsign].path = line;
            }
        }
        return index;
    }
        
    // takes a list of prediction points, callsign and sim run num
    function addPrediction(callsign, simRun, prediction) {
        
        if (!(predLines.hasOwnProperty(callsign))) {
            
            predLines[callsign] = [];
        }
        if (predLines[callsign][simRun] && predLines[callsign][simRun].pathLength() > 0) {
            // if there is already a line for this run
            removeMapItem(predLines[callsign][simRun]);
        }
        var newLine = predLineComp.createObject(map);
        
        predLines[callsign][simRun] = newLine;
        addMapItem(newLine);

        for (var i = 0; i < prediction.length; i++) {
            predLines[callsign][simRun].addCoordinate(QtPositioning.coordinate(prediction[i]["lat"], prediction[i]["lng"]));
        }
    }

    function clearPredictions(callsign) {
        if (predLines.hasOwnProperty(callsign)) {
            for (var i = 0; i < predLines[callsign].length; i++) {
                removeMapItem(predLines[callsign][i]);
                predLines[callsign][i].destroy();
                removeMapItem(predDots[callsign][i]["burst"]);
                predDots[callsign][i]["burst"].destroy();
                removeMapItem(predDots[callsign][i]["landed"]);
                predDots[callsign][i]["landed"].destroy();
            }
            predLines[callsign] = [];
        }
    }
    // function to plot a special occurence on the prediction line i.e. a bursting or landing
    // take callsign, sim run and dictionary with lat, lng, alt and event type
    function addPredictionEvent(callsign, simRun, event) {
        if (!(predDots.hasOwnProperty(callsign))) {
            
            predDots[callsign] = [];
        }

        if (predDots[callsign][simRun] && predDots[callsign][simRun].hasOwnProperty(event["type"])) {
            // if there is already a dot of this type for this run
            removeMapItem(predDots[callsign][simRun][event["type"]]);
        }
        var newDot = predDotComp.createObject(map, {"coordinate.latitude": event["lat"], 
                                                    "coordinate.longitude": event["lng"],
                                                    "text": callsign+" "+event["type"]+"@\nlat:"+event["lat"]+"\nlng: "+event["lng"]+"\nalt: "+event["alt"]+"m"+
                                                    "\non sim "+(simRun+1)});
        addMapItem(newDot);
        if (!(predDots[callsign].hasOwnProperty(simRun))) {
            // if there is no dictionary for this run yet
            predDots[callsign][simRun] = [];
        }
        predDots[callsign][simRun][ event["type"] ] = newDot;
    }

    function locVisible(callsign, isVisible) {
        if (!(callsign in locLines)) {return;}
        
        var setter;
        if (isVisible) {
            setter = addMapItem
        }else{
            setter = removeMapItem
        }
        setter(locLines[callsign])

        for (var i=0; i < locDots[callsign].length; i++) {
            setter(locDots[callsign][i])
        }
    }
    function simVisible(callsign, isVisible) {
        if (!(callsign in predDots)) {return;}
        
        var setter;
        if (isVisible) {
            setter = addMapItem
        }else{
            setter = removeMapItem
        }

        for (var i=0; i < predLines[callsign].length; i++) {
            setter(predLines[callsign][i])
        }

        for (var i=0; i < predDots[callsign].length; i++) {

            setter(predDots[callsign][i]["burst"])
            setter(predDots[callsign][i]["landed"])
        }
    }
    function heatVisible(callsign, showing) {
        if (callsign in predLines) {
            heatImg.visible = showing;
            heatImg.source = "balloon.png"      //this is just to force a reload of the image if a new sim has been run
            heatImg.source = "sim_data/"+callsign+".png"
        }
    }
    function setHeatmap(callsign, lat, lng, resolution) {
        // used by backend
        heatmap.coordinate = QtPositioning.coordinate(lat, lng)
        heatmap.zoomLevel = Math.log2(resolution*360/256)   // using OSM definition
    }
    function setCurrBalloon(callsign) {
        currBalloon = callsign;
    }

    MapQuickItem {
        id: heatmap
        anchorPoint.x: heatImg.width/2
        anchorPoint.y: heatImg.height/2
        coordinate: QtPositioning.coordinate(-32.266667, 150.9 )
        zoomLevel: 1
        sourceItem: Image {
            id: heatImg
            visible: false
            cache: false
        }
    }
    MouseArea {
        // used to track mouse as it moves around the map
        // probably wouldn't work if the map moved off the top left corner
        id: mArea
        anchors.fill: map
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        hoverEnabled: true
        Text {
            id: mText
            property var coord: map.toCoordinate(Qt.point(mArea.mouseX,mArea.mouseY))
            text: "lat: "+coord.latitude.toFixed(4)+" lng: "+coord.longitude.toFixed(4)
            font.pixelSize: 20
        }

        onClicked: {
            if (mouse.button & Qt.LeftButton) {
                clickMenu.popup()
                menuText.text = mText.text
            }
        }
        Menu {
            id: clickMenu
            property var lat: 0
            property var lng: 0
            onOpened : {
                lat = mText.coord.latitude;
                lng = mText.coord.longitude;
            }
            function quickSim(termination) {

                var alt = parseFloat(altTxt.text);
                if (isNaN(alt)) {
                    console.log("Invalid input for quick sim: "+altTxt.text)
                }else{
                    eval(map.currBalloon+".runQuickSim("+lat+","+lng+","+alt+","+termination+")");
                }
            }
            ColumnLayout {
                Text {
                    id: menuText
                }
                TextField {
                    id: altTxt
                    placeholderText: "Alt (m)"
                    maximumLength: 5
                    selectByMouse: true
                }
                Button {
                    text: "Simulate flight"
                    onClicked: {
                        clickMenu.quickSim(false);
                        clickMenu.close()
                    }
                }
                Button {
                    text: "Simulate termination"
                    onClicked: {
                        clickMenu.quickSim(true);
                        clickMenu.close()
                    }
                }
            }
        }
    }
    Shortcut {
        sequence: "up"
        onActivated : map.center.latitude+=0.1*1/map.zoomLevel;
    }
    Shortcut {
        sequence: "down"
        onActivated : map.center.latitude-=0.1*1/map.zoomLevel;
    }
    Shortcut {
        sequence: "left"
        onActivated : map.center.longitude-=0.1*1/map.zoomLevel;
    }
    Shortcut {
        sequence: "right"
        onActivated : map.center.longitude+=0.1*1/map.zoomLevel;
    }
    Shortcut {
        sequence: "Pgup"
        onActivated : map.zoomLevel+=1;
    }
    Shortcut {
        sequence: "Pgdown"
        onActivated : map.zoomLevel-=1
    }
}
