import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

import base64
import logging
from apiclient import errors
import threading
import time
import datetime

class IridiumEmailWatcher() :
    """
    How to use:
    Initialise with a Gmail email and a refresh period in seconds for checking for new emails
    Call .start(callback) with callback being the function you wish the dictionary of values to be passed to
    """

    def __init__(self, email, period) :

        logging.disable(level=50)
        creds = None
        SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('pymap/token.pickle'):
            with open('pymap/token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file('pymap/credentials.json', SCOPES)
                creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('pymap/token.pickle', 'wb') as token:
            pickle.dump(creds, token)

        self._service = build('gmail', 'v1', credentials=creds)
        self._email = email
        self._period = period
        self._lastMsg = None
        self._lastHistory = '1'

    def ParseEmail(self, msg) :
        """
        Breaks plaintext message into string-representation dictionary based on colon separation
        Assumes lines are terminated with \r\n
        """

        lines = msg.split("\r\n")
        result = {}
        for l in lines :
            if len(l) < 2 :
                continue
            firstColon = l.find(":")
            result[l[:firstColon]] = l[firstColon+1:]
        
        result["lat"] = result.pop("Iridium Latitude")
        result["lng"] = result.pop("Iridium Longitude")
        result["time"] = datetime.datetime.strptime(result["Transmit Time"], "%Y-%m-%d %H:%M:%S UTC").timestamp() + time.timezone
        del result["Transmit Time"]
        return result

    def GetMessage(self, msg_id):
        """Get a Message with given ID.

        Args:
            service: Authorized Gmail API service instance.
            user_id: User's email address. The special value "me"
            can be used to indicate the authenticated user.
            msg_id: The ID of the Message required.

        Returns:
            A Message.
        """
        try:
            message = self._service.users().messages().get(userId=self._email, id=msg_id).execute()

            return message
        except errors.HttpError as error:
            print('An error occurred: %s' % error)
            return None

    def GetSubjectLine(self, message) :

        for h in message["payload"]["headers"] :
            if h.get("name") == "Subject" :
                return h["value"]
        
        return None
    
    def GetBodyText(self, message) :

        return base64.b64decode(message["payload"]["body"]["data"]).decode("utf-8")

    def _update(self) :

        while True :
            history = self._service.users().history().list(userId=self._email,startHistoryId=self._lastHistory).execute()

            history = history.get("history")

            if history != None:
                # need actual entries
                history = sorted(history, key = lambda i: i["id"], reverse=True)
                recordIndex = 0

                newRecord = True
                while history[recordIndex].get("messagesAdded") == None :
                    #look for the last received message
                    recordIndex += 1

                    if recordIndex >= len(history) :
                        newRecord = False
                        break
                
                if newRecord :
                    msgId = history[recordIndex]["messages"][0]["id"]
                    self._lastHistory = history[recordIndex]["id"]
                    
                    message = self.GetMessage(msgId)

                    if self._lastMsg != message :
                        self.target(self.ParseEmail(self.GetBodyText(message)))
                        self._lastMsg = message

            time.sleep(self._period)

    def start(self, target) :

        self.target = target
        threading.Thread(target=self._update, daemon=True).start()