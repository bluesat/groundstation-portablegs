// Contains markup for a dot on the map

import QtQuick 2.0
import QtQuick.Controls 2.5
import QtLocation 5.6
import QtPositioning 5.6 
MapQuickItem {
    id: dot
    property var text: ""
    property var timestamp: 0 // used to keep in order
    property var alt: 0       // used to help with altitude change colouring
    sourceItem: Rectangle {
        id: rect
        width: 10
        height: 10
        color: "white"
        border.width: 2
        border.color: "blue"
        smooth: true
        radius: 3
        //property var rotation: 45
        //transform: Rotation { origin.x: width/2; origin.y: height/2; angle: rotation}
    }
    coordinate {
        latitude: 0
        longitude: 0
    }
    zoomLevel: 0.0
    opacity: 1.0
    anchorPoint: Qt.point(sourceItem.width / 2, sourceItem.height / 2)

    MouseArea {
        anchors.fill: parent
        preventStealing: true
        hoverEnabled: true
        
        ToolTip {
            visible: parent.containsMouse
            // references the MapQuickItem property text
            text: dot.text
        }

    }
    function setBorderColor(colorString) {
        rect.border.color = colorString;
    }
    function setAppearance(source) {
        //only meant to be called once after the dot has been created
        switch(source) {
        case "APRS":
            //rect.rotation = 0;
            rect.color = "orange";
            break;
        case "Iridium":
            rect.radius = 10;
            break;
        case "SPOT":
            rect.radius = 10;
            rect.color = "green";
            break;
        }
    }
}