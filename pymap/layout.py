#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PySide2 import QtCore    
from PySide2.QtWidgets import QApplication
from PySide2.QtQuick import QQuickView, QQuickItem
from PySide2.QtCore import QUrl, QObject, QThread
from PySide2.QtGui import QIcon
import threading
import zmq
import random
from time import sleep
import os
import json
import multiprocessing
from astra.simulator import forecastEnvironment, flight
from datetime import datetime, timedelta
from time import sleep
from heatmap import Heatmap
from IridiumEmailWatcher import IridiumEmailWatcher
from APRSWatcher import APRSWatcher
from watchdog.observers import Observer
from aprs_receiver.run_aprs import AprsRfReceiver

class Balloon(QtCore.QObject):
    
    # signal to update UI map with the new location
    # this must stay defined in the class, not in __init__
    newLoc = QtCore.Signal(dict)

    # signal to provide a complete prediction path
    prediction = QtCore.Signal(str, int, list)

    #signal to clear predictions off the map
    clearPredictions = QtCore.Signal(str)

    # signal to mark a significant point on a prediction path (bursting, landing)
    predictionEvent = QtCore.Signal(str, int, dict)

    # give parameters for displaying the heatmap correctly
    setHeatmap = QtCore.Signal(str, float, float, float)

    # initialise the controls for the balloon
    addBalloon = QtCore.Signal(str)

    # send flight sim parameters to be shown on the user form
    # str: name of ballon. dict: data item:value pairs
    giveSimParameters = QtCore.Signal(str, dict)

    # give the current settings of the simulation to the UI
    giveState = QtCore.Signal(str, dict)

    # signal that a simulation is underway
    signalSimulating = QtCore.Signal(bool)

    def __init__(self, name="default", aprsCallsigns="", iridiumEmail=""):
        QtCore.QObject.__init__(self)

        disallowed_chars = " /"
        for c in disallowed_chars :
            if c in name :
                name = "INVALID_NAME_GIVEN"
                break
            
        self._balloonID = name       # name of balloon

        self._runningSim = False    # flag to indicate if a sim is running, to prevent multiple being run simultaneously
        self._newLoc = False        # flag to indicate if the current location is ahead of the current sim environment

        self._simParameters = {"launchLat": -32.266667,
                               "launchLng": 150.9,
                               "launchElev": 157,
                               "inflateTemp": 20,
                               "nozzleLift": 8,
                               "payloadMass": 1.5,
                               "numSims": 4,
                               "trainSphereDia": .2,
                               "useLive": False}
        self._currState = {"launchLat": -32.266667,
                           "launchLng": 150.9,
                           "launchElev": 157}

        self._lastTime = 0

        if iridiumEmail != "" :
            self._iridiumWatcher = IridiumEmailWatcher(iridiumEmail, 20)
            self._iridiumWatcher.start(self.handleIridiumDict)
        if aprsCallsigns != "":
            self._aprsWatcher = APRSWatcher(aprsCallsigns, 10)
            self._aprsWatcher.start(self.handleAPRSfiDict)
        
        #self._aprsReceiver = AprsRfReceiver(os.getcwd()+"/pymap/aprs_receiver")
        #self._aprsReceiver.start(self.handleAPRSRadioDict)

        self._reloadEnvironment()
        # this must be last to ensure proper object setup
        thread = threading.Thread(target=self._update, daemon=True)
        thread.start()

    @property
    def balloonID(self) :
        return self._balloonID

    def _reloadEnvironment(self) :

        if self._simParameters["useLive"] :
            parameters = self._currState
        else :
            parameters = self._simParameters

        self._simEnvironment = forecastEnvironment(launchSiteLat=parameters["launchLat"],         # deg
                                            launchSiteLon=       parameters["launchLng"],         # deg
                                            launchSiteElev=      parameters["launchElev"]  ,      # m
                                            inflationTemperature=self._simParameters["inflateTemp"],       # degC
                                            dateAndTime=datetime.now() + timedelta(hours=2),#datetime.strptime('Aug 12 2019  1:00PM', '%b %d %Y %I:%M%p')
                                            UTC_offset=0,
                                            forceNonHD=False,
                                            debugging=False)

        # load locally stored weather predictions
        # assumes is being run from the root project folder
        files = {"tmpprs":os.getcwd()+"/pymap/weather_data/tmpprs.txt",
                 "hgtprs":os.getcwd()+"/pymap/weather_data/hgtprs.txt",
                 "ugrdprs":os.getcwd()+"/pymap/weather_data/ugrdprs.txt",
                 "vgrdprs":os.getcwd()+"/pymap/weather_data/vgrdprs.txt"}
        self._simEnvironment.loadFromNOAAFiles(files)

        self._newLoc = False

    #TODO: change to to subscribe to zmq updates
    def _update(self):
        context = zmq.Context()
        socket = context.socket(zmq.SUB)
        socket.connect ("tcp://localhost:5556")
        socket.setsockopt(zmq.SUBSCRIBE, b'')
        lng = 150.9
        lat = -32.26666
        alt = 10000
        time = datetime.now().timestamp()
        sources = ["Iridium", "APRS", "SPOT"]
        sleep(2) # ensure QML is ready
        while True:
            
            time += 2
            # #aprsmsg = socket.recv_pyobj()
            source = int(random.random()*3)
            self.insertLoc(lat, lng, alt, time, sources[source])

            # if (self._balloonID == "Balloon_1") :
            #     lng += 0.1
            # else :
            #     lat += 0.1
            lat += .001*random.random()
            lng += .001*random.random()
            alt += 20*(random.random()-0.5)
            
            sleep(2)

    def _isValidPosition(self, lat, lng, alt) :
        nswTopLeft = (-28.25, 141)
        nswBottomRight = (-37.68, 154)

        return lat < nswTopLeft[0] and lat > nswBottomRight[0] and lng > nswTopLeft[1] and lng < nswBottomRight[1] and alt >= 0 and alt < 100000

    def handleIridiumDict(self, dic) :
        alt = 10
        self.insertLoc(dic["lat"], dic["lng"], alt, dic["time"], "Iridium")
    
    def handleAPRSfiDict(self, dic) :
        alt = dic.get("alt",0)
        self.insertLoc(dic["lat"], dic["lng"], alt, dic["time"], "APRS", dic["name"])
    
    def handleAPRSRadioDict(self, dic) :
        pass

    def insertLoc(self, lat, lng, alt, time, source="Balloon", id=""):
        '''Insert data from a new ping into the object, as well as display it on the map'''
        '''lat, lng, alt should be floats, time should be an int (UNIX timestamp, local time)'''

        if self._isValidPosition(lat, lng, alt) :
            
            data = {"callsign":self.balloonID, "lat":lat, "lng":lng, "alt":alt, 
                    "time": datetime.fromtimestamp(time).strftime('%H:%M:%S'),
                    "timestamp" : time,
                    "source": source, "id": id}
            
            if time > self._lastTime :
                self._currState["launchLat"] = lat
                self._currState["launchLng"] = lng
                self._currState["launchElev"] = alt
                self._lastTime = time
                self._newLoc = True

            self.newLoc.emit(data)
            
        else :
            print(f"rejected position lat: {lat} lng: {lng} alt: {alt} src: {source} ({id})")

    @QtCore.Slot()
    def runSimulation(self) :
        threading.Thread(target=self._simulation).start()
    
    @QtCore.Slot()
    def runTerminationSim(self) :
        threading.Thread(target=self._simulation,args=(True,)).start()
    
    @QtCore.Slot(float, float, float, bool)
    def runQuickSim(self, lat, lng, alt, isTermination) :
        self._simParameters["useLive"] = False
        self._simParameters["launchLat"] = lat
        self._simParameters["launchLng"] = lng
        self._simParameters["launchElev"] = alt
        self._reloadEnvironment()
        threading.Thread(target=self._simulation,args=(isTermination,)).start()
        
    def _simulation(self, isTermination=False) :
        if (self._runningSim) :
            return
        self._runningSim = True
        self.signalSimulating.emit(True)

        if self._newLoc :
            self._reloadEnvironment()
        # Launch setup
        simFlight = flight(environment=     self._simEnvironment,
                       balloonGasType=      'Helium',
                       balloonModel=        'HW2000',
                       nozzleLift=          self._simParameters["nozzleLift"],                                # kg
                       payloadTrainWeight=  self._simParameters["payloadMass"],                    # kg
                       parachuteModel=      'TX160',
                       numberOfSimRuns=     self._simParameters["numSims"],
                       isTermination=       isTermination,
                       trainEquivSphereDiam=self._simParameters["trainSphereDia"],                    # m
                       floatingFlight=      False,
                       outputFile=os.path.join('.', f"pymap/sim_data/{self._balloonID}-sim.json"),
                       debugging=           False,
                       log_to_file=         False)

        simFlight.run()
        self.showSimulation()

    def _saveArchive(self, toArchive) :

        # save a copy
        toArchive["Launch Date"] = self._simEnvironment.dateAndTime.strftime("%d-%m-%Y, %H:%M")
        toArchive["Simulation time"] = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
        toArchive["Balloon"] = self._balloonID
        toArchive["Forecast model"] = "date when forecast was issued" #TODO: put a date here
        toArchive["Used live balloon pos"] = self._simParameters["useLive"]

        for key, value in self._simParameters.items() :
            toArchive[key] = value

        archiveName = self._balloonID + ":" + toArchive["Simulation time"] + ".json"

        archiveDir = f"pymap/sim_data/archive/{self._balloonID}"
        if not os.path.exists(archiveDir):
            os.makedirs(archiveDir)

        with open(archiveDir+"/"+archiveName, "w") as archive_json :
            json.dump(toArchive, archive_json)
        
    @QtCore.Slot()
    def showSimulation(self) :

        self.clearPredictions.emit(self._balloonID)
        try :
            with open(f"pymap/sim_data/{self._balloonID}-sim.json", 'r') as f :
                sims = json.load(f)
            
            #dictionary with only landing points : used for the heatmap
            landings = {"points": [None]*self._simParameters["numSims"]}
            
            for i in range(0,self._simParameters["numSims"]) :
                self.prediction.emit(self._balloonID, i, sims["flightPaths"][i]["points"])

                burstEvent = {"type":"burst"}
                landedEvent = {"type":"landed"}

                event = {**sims["burstMarkers"][i], **burstEvent}
                self.predictionEvent.emit(self._balloonID, i, event)

                event = {**sims["flightPaths"][i]["points"][-1], **landedEvent}
                self.predictionEvent.emit(self._balloonID, i, event)

                landings["points"][i] = sims["flightPaths"][i]["points"][-1]
                
            with open(f"pymap/sim_data/{self._balloonID}-landings.json", 'w') as landings_json:
                json.dump(landings, landings_json)
            
            map = Heatmap(300)
            map.generate(f"pymap/sim_data/{self._balloonID}.png", f"pymap/sim_data/{self._balloonID}-landings.json")
            self.setHeatmap.emit(self._balloonID, map.centreAnchor[0], map.centreAnchor[1], map.resolution)

            self._saveArchive(sims)
        except :
            print("exception during simulation displaying")
        finally :
            self._runningSim = False
            self.signalSimulating.emit(False)

    @QtCore.Slot()
    def requestSimParameters(self) :
        self.giveSimParameters.emit(self._balloonID, self._simParameters)
    
    @QtCore.Slot('QVariant')
    def setSimParameters(self, params) :
        self._simParameters = {"launchLat": params.property("launchLat").toNumber(),
                               "launchLng": params.property("launchLng").toNumber(),
                               "launchElev": params.property("launchElev").toNumber(),
                               "inflateTemp": params.property("inflateTemp").toNumber(),
                               "nozzleLift": params.property("nozzleLift").toNumber(),
                               "payloadMass": params.property("payloadMass").toNumber(),
                               "numSims": params.property("numSims").toInt(),
                               "trainSphereDia": params.property("trainSphereDia").toNumber(),
                               "useLive": params.property("useLive").toBool()}
        self._reloadEnvironment()

    def connectToFrontend(self, root) :

        mapItem = root.findChild(QQuickItem, "map")
        # connect update signals to qml functions
        self.newLoc.connect(mapItem.addDot)
        self.prediction.connect(mapItem.addPrediction)
        self.predictionEvent.connect(mapItem.addPredictionEvent)
        self.setHeatmap.connect(mapItem.setHeatmap)
        self.clearPredictions.connect(mapItem.clearPredictions)

        formItem = root.findChild(QQuickItem, "background")
        self.addBalloon.connect(formItem.addBalloon)
        self.addBalloon.emit(self._balloonID)
        self.signalSimulating.connect(formItem.setSimulatingStatus)

        self.giveSimParameters.connect(formItem.giveSimParameters)
        view.rootContext().setContextProperty(self._balloonID, self)

app = QApplication([])
view = QQuickView()
url = QUrl("./pymap/UiForm.qml")
view.setSource(url)

app.setWindowIcon(QIcon("icon.png"))
app.setApplicationName("Groundstation")

root = view.rootObject()

balloon = Balloon("Balloon_1")#, ["VK2KWT-5","VK2KWT-10"])#, "bluesatiridium@gmail.com")     # carries all the data for the balloon
# Callsigns MAY NOT (yet) have spaces: use underscores

balloon.connectToFrontend(root)

view.show()

app.exec_()
