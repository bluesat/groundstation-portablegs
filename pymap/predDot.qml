// Contains markup for a dot on the map

import QtQuick 2.0
import QtQuick.Controls 2.5
import QtLocation 5.6
import QtPositioning 5.6 
MapQuickItem {
    id: predDot
    property var text: ""
    sourceItem: Rectangle {
        width: 5
        height: 5
        color: "orange"
        border.width: 1
        border.color: "blue"
        smooth: true
        radius: 15
    }
    coordinate {
        latitude: 0
        longitude: 0
    }

    opacity: 1.0
    anchorPoint: Qt.point(sourceItem.width / 2, sourceItem.height / 2)

    MouseArea {
        anchors.fill: parent

        hoverEnabled: true
        
        ToolTip {
            visible: parent.containsMouse
            // references the MapQuickItem property text
            text: predDot.text
        }

    }
}